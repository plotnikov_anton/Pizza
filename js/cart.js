/**
 * @author Anton Plotnikov
 * @description {@link Cart} rendering script
 */

/**
 *
 * @constructor
 */
function Cart() {

    let instance;
    let self = this;
    let storage = window.localStorage;
    let cartData = JSON.parse(storage.getItem('cart'));
    let enabled = true;

    /**
     * @description  component initialization
     * @returns {*} new Singleton instance
     */
    function createElement() {

        if (!instance) {
            render();
        }
        return instance;
    }

    /**
     * @description main component rendering function
     */
    function render() {

        instance = document.createElement('section');
        instance.className = 'b-cart';
        instance.setAttribute('id', 'cart');

        instance.onclick = function (event) {

            let target = event.target;
            let action = target.getAttribute('data-action');

            if (action) {
                self[action](target);
            }
        }
    }

    /**
     * @description items rendering function
     */
    function renderItems() {

        if (Object.keys(cartData).length !== 0) {

            let table = document.createElement('table');
            table.className = 'b-table';

            let trHead = document.createElement('tr');
            trHead.className = 'b-tr';

            let thName = document.createElement('th');
            thName.innerText = 'Pizza';
            thName.className = 'b-tr__th';

            let thPrice = document.createElement('th');
            thPrice.innerText = 'Price';
            thPrice.className = 'b-tr__th';

            let thCalories = document.createElement('th');
            thCalories.innerText = 'Calories';
            thCalories.className = 'b-tr__th';

            let thIngredients = document.createElement('th');
            thIngredients.innerText = 'Ingredients';
            thIngredients.className = 'b-tr__th';

            let thQuantity = document.createElement('th');
            thQuantity.innerText = 'Quantity';
            thQuantity.className = 'b-tr__th';

            trHead.appendChild(thName);
            trHead.appendChild(thPrice);
            trHead.appendChild(thCalories);
            trHead.appendChild(thIngredients);
            trHead.appendChild(thQuantity);

            table.appendChild(trHead);

            let values = Object.values(cartData);

            values.forEach((item) => {

                let quantity = "1";

                let tr = document.createElement('tr');
                tr.className = 'b-tr';

                let tdName = document.createElement('td');
                tdName.innerText = item.name;
                tdName.className = 'b-tr__td';

                let tdPrice = document.createElement('td');
                tdPrice.innerText = item.price;
                tdPrice.className = 'b-tr__td';
                tdPrice.classList.add('price');

                let tdCalories = document.createElement('td');
                tdCalories.innerText = item.calories;
                tdCalories.className = 'b-tr__td';

                let tdIngredients = document.createElement('td');
                tdIngredients.innerText = item.ingredients;
                tdIngredients.className = 'b-tr__td';

                let tdQuantity = document.createElement('td');
                tdQuantity.setAttribute('data-cell', 'quantity');
                tdQuantity.innerText = quantity;
                tdQuantity.className = 'b-tr__td';
                tdQuantity.classList.add('quantity');

                let addCell = document.createElement('td');
                addCell.className = 'b-tr__td';

                let addButton = document.createElement('button');
                addButton.className = 'b-tr__cart-button';
                addButton.setAttribute('data-action', 'incrementItem');
                addButton.innerText = '+';
                addCell.appendChild(addButton);
                addCell.appendChild(document.createElement('br'));

                let removeButton = document.createElement('button');
                removeButton.className = 'b-tr__cart-button';
                removeButton.setAttribute('data-action', 'decrementItem');
                removeButton.setAttribute('data-id', item.id);
                removeButton.innerText = '-';
                addCell.appendChild(removeButton);

                tr.appendChild(tdName);
                tr.appendChild(tdPrice);
                tr.appendChild(tdCalories);
                tr.appendChild(tdIngredients);
                tr.appendChild(tdQuantity);
                tr.appendChild(addCell);
                table.appendChild(tr);
            });

            instance.appendChild(table);
            renderButtons();
        }
        else {
            generateMessage();
        }
    }

    function renderButtons() {

        let buttonContainer = document.createElement('div');
        buttonContainer.className = 'b-cart-buttons';

        let clearText = document.createElement('p');
        clearText.className = 'b-cart-buttons__text';
        clearText.innerText = `Clear cart:\n`;

        let clearButton = document.createElement('button');
        clearButton.className = 'b-cart__clear-button';
        clearButton.setAttribute('data-action', 'clearCart');

        let clearIcon = document.createElement('i');
        clearIcon.className = 'material-icons';
        clearIcon.setAttribute('data-action', 'clearCart');
        clearIcon.innerText = 'remove_shopping_cart';

        clearButton.appendChild(clearIcon);

        let orderText = document.createElement('p');
        orderText.className = 'b-cart-buttons__text';
        orderText.innerText = `Check the order:\n`;

        let orderButton = document.createElement('button');
        orderButton.className = 'b-cart-buttons__order-button';
        orderButton.setAttribute('data-action', 'checkOrder');
        orderButton.innerText = `\u2714`;

        buttonContainer.appendChild(orderText);
        buttonContainer.appendChild(orderButton);

        buttonContainer.appendChild(clearText);
        buttonContainer.appendChild(clearButton);

        instance.appendChild(buttonContainer);
    }

    /**
     * @description a function to clear all the content
     */
    function clearItems() {

        document.getElementById('cart').innerHTML = "";
    }

    /**
     * @description a function to clear the specific item
     * @param item an item to be cleared
     */
    function clearItem(item) {

        item.innerHTML = "";
    }

    /**
     * @description a function to show the customer the total price and
     * quantity
     * @param target
     */
    function checkOrder(target) {

        if (enabled) {
            let parent = target.parentElement;
            let tableData = parent.previousElementSibling;

            let prices = tableData.getElementsByClassName('price');
            let quantities = tableData.querySelectorAll('.quantity');

            let totalPrice = 0;
            let totalQuantity = 0;

            for (let i = 0; i < prices.length; i++) {

                totalPrice += parseInt(prices[i].innerText) * parseInt(quantities[i].innerText);
                totalQuantity += parseInt(quantities[i].innerText);
            }

            let orderData = document.createElement('div');
            orderData.className = 'b-order-info';

            let orderText = document.createElement('p');
            orderText.className = 'b-order-info__text';
            orderText.innerText = `Pizzas: ${totalQuantity} units\nSum to pay: ${totalPrice} uah`;

            let orderCloseButton = document.createElement('button');
            orderCloseButton.className = 'b-order-info__close-button';
            orderCloseButton.setAttribute('data-action', 'closeElement');
            orderCloseButton.innerText = `\u2716`;

            orderData.appendChild(orderText);
            orderData.appendChild(orderCloseButton);

            tableData.style.opacity = '0.02';
            instance.appendChild(orderData);
            enabled = false;
        }
    }

    /**
     * @description a function to close the element
     * @param target
     */
    function closeElement(target) {

        let parent = target.parentElement;
        let tableData = instance.firstChild;

        parent.style.display = 'none';
        tableData.style.opacity = '1';
        enabled = true;
    }

    /**
     * @description a function to increment the quantity
     * @param target
     */
    function incrementItem(target) {
        if (enabled) {
            let parent = target.parentElement.previousElementSibling;
            parent.innerText = parseInt(parent.innerText) + 1;
        }
    }

    /**
     * @description a function to decrement the quantity
     * In case of deleting the very last item of a specific type,
     * localStorage is set to remove the item from cart properties
     * @param target
     */
    function decrementItem(target) {
        if (enabled) {
            let parent = target.parentElement.previousElementSibling;
            let quantity = parseInt(parent.innerText);
            let id = parseInt(target.getAttribute('data-id'));

            if (quantity > 1) {
                parent.innerText = quantity - 1;
            }
            else {
                clearItem(parent.parentElement);
                delete cartData[id];
                storage.setItem('cart', JSON.stringify(cartData));
            }
        }
    }

    /**
     * @description a function which sets an empty cart to
     * localStorage
     */
    function clearCart() {

        storage.setItem('cart', JSON.stringify({}));
        clearItems();
        generateMessage();
    }

    /**
     * @description a function to generate a message in case that the cart is empty
     */
    function generateMessage() {

        let message = document.createElement('div');
        message.className = 'b-cart-message';

        let messageText = document.createElement('p');
        messageText.className = 'b-cart-message__text';
        messageText.innerText = `Your cart is empty\nBack to the shop:`;

        let shoppingLink = document.createElement('a');
        shoppingLink.className = 'b-cart-shopping-link';
        shoppingLink.setAttribute('href', 'grid.html');

        let shoppingIcon = document.createElement('i');
        shoppingIcon.className = 'material-icons';
        shoppingIcon.innerText = 'shopping_basket';

        shoppingLink.appendChild(shoppingIcon);
        message.appendChild(messageText);
        message.appendChild(shoppingLink);
        instance.appendChild(message);
    }

    this.createElement = createElement;
    this.renderItems = renderItems;
    this.clearItems = clearItems;
    this.incrementItem = incrementItem;
    this.decrementItem = decrementItem;
    this.clearCart = clearCart;
    this.checkOrder = checkOrder;
    this.closeElement = closeElement;
}

let cart = new Cart();
let element = cart.createElement();
cart.renderItems();

let footer = document.getElementById('footer');
document.body.insertBefore(element, footer);

/**
 * @author Anton Plotnikov
 * @description {@link Grid} rendering script
 */

/**
 *
 * @constructor
 */
function Grid() {

    let instance;
    let currentPage = 0;
    let self = this;
    let items = initItems();
    let storage;
    let customState;
    const itemsNumber = 6;

    /**
     * @description  component initialization
     * @returns {*} new Singleton instance
     */
    function createElement() {

        if (!instance) {
            render();
        }
        return instance;
    }

    /**
     * @description main component rendering function
     */
    function render() {

        instance = document.createElement('section');
        instance.className = 'b-grid';
        instance.setAttribute('id', 'grid');

        instance.onclick = function (event) {

            let target = event.target;
            let action = target.getAttribute('data-action');

            if (target.hasAttribute('data-pizza')) {

                self[action](target);

            } else if (action) {

                self[action]();
            }

        };

        instance.oninput = function (event) {

            if (event.target.closest('.b-button-container')) {
                filterIngredients();
            }
        };
    }

    /**
     * @description items rendering function
     * @param items - an array of objects to be represented
     * in grid
     */
    function renderItems(items) {

        let row = document.createElement('div');
        row.className = 'b-row';

        items.forEach((item, index) => {

            let customPrice = 50;
            let customCalories = 0;

            let article = document.createElement('article');
            article.className = 'b-article';

            if (index < itemsNumber) {
                article.classList.add('shown');
                article.style.display = 'block';
            }

            let photoContainer = document.createElement('div');
            photoContainer.className = 'b-photo-container';

            let photo = document.createElement('img');
            photo.className = 'b-photo-container__img';
            photo.style.width = '100%';
            photo.setAttribute('src', item.photo);

            let header = document.createElement('h1');
            header.className = 'b-pizza-name';
            header.innerText = item.name;
            header.setAttribute('data-id', `name-${index}`);

            let container = document.createElement('div');
            container.className = 'b-description-container';

            let list = document.createElement('ul');
            list.className = 'b-description-container-list';

            let customContainer = document.createElement('div');
            customContainer.className = 'b-description-container-custom';

            let customList = document.createElement(('ul'));
            customList.className = 'b-description-container-custom-list';
            customList.classList.add('custom');

            let newPrice = document.createElement('li');
            newPrice.className = 'b-description-container-custom-list__li';

            let newIngredients = document.createElement('li');
            newIngredients.className = 'b-description-container-custom-list__li-ingredients';
            newIngredients.style.display = 'none';
            let customIngredients = new Set();

            let newCalories = document.createElement(('li'));
            newCalories.className = 'b-description-container-custom-list__li';

            customList.appendChild(newPrice);
            customList.appendChild(newIngredients);
            customList.appendChild(newCalories);

            let customButton = document.createElement('button');
            customButton.className = 'b-button-custom';
            customButton.style.background = '#E94B3C';
            customButton.setAttribute('data-action', 'toggleCustom');
            customButton.innerText = "Customize";

            let addToCartButton = document.createElement('button');
            addToCartButton.className = 'b-button-to-cart';
            addToCartButton.innerText = '+';
            addToCartButton.setAttribute('data-action', 'addToCart');
            addToCartButton.setAttribute('data-pizza', `${index}`);

            customContainer.appendChild(customButton);
            customContainer.appendChild(customList);
            customContainer.appendChild(addToCartButton);

            container.appendChild(list);
            container.appendChild(customContainer);

            let info = new Map(Object.entries(item));
            info.delete('photo');
            info.delete('name');
            info.delete('toString');

            info.forEach((value, key) => {

                if (key !== 'id') {
                    let li = document.createElement('li');
                    li.className = 'b-description-container-custom-list__li';

                    if (typeof value === 'object') {

                        let form = document.createElement('form');
                        form.className = 'b-ingredients-form';

                        value.forEach((item) => {

                            let p = document.createElement('p');
                            p.className = 'b-ingredients-p';
                            let span = document.createElement('span');
                            span.className = "b-ingredients-p__description";
                            span.innerText = item.name + " ";
                            p.appendChild(span);

                            let label = document.createElement('label');
                            label.className = 'b-ingredients-label';
                            label.classList.add('custom');

                            let checkbox = document.createElement('input');
                            checkbox.className = 'b-ingredients-label__checkbox';
                            checkbox.setAttribute('type', 'checkbox');

                            let slider = document.createElement('span');
                            slider.className = 'b-ingredients-label__slider';

                            label.appendChild(checkbox);
                            label.appendChild(slider);
                            p.appendChild(label);
                            form.appendChild(p);

                            /**
                             * @description Checking whether the ingredient was chosen or not
                             */
                            checkbox.onclick = function () {

                                if (customState) {
                                    if (checkbox.checked === true) {
                                        customPrice += item.price;
                                        customCalories += item.calories;
                                        customIngredients.add(item.name);
                                    } else {
                                        customPrice -= item.price;
                                        customCalories -= item.calories;
                                        customIngredients.delete(item.name);
                                    }
                                    newPrice.innerText = `Price:\u00A0` + customPrice;
                                    newIngredients.innerText = Array.from(customIngredients).toString().replace(/,/g, `\n`);
                                    newCalories.innerText = `Calories:\u00A0` + customCalories;
                                }
                            };
                        });

                        li.innerText = `${key.replace(key.charAt(0), function (string) {
                            return string.charAt(0).toUpperCase();
                        })}:\u00A0`;
                        li.appendChild(form);
                    } else {

                        li.innerText = `${key.replace(key.charAt(0), function (string) {
                            return string.charAt(0).toUpperCase();
                        })}:\u00A0${value}`;
                    }
                    li.classList.add(`${key}`);
                    list.appendChild(li);
                }
            });

            article.appendChild(photoContainer)
                .appendChild(photo);
            article.appendChild(header);
            article.appendChild(container);

            row.appendChild(article);
        });

        element.appendChild(row);
    }

    let elements = document.getElementsByClassName('b-article');

    /**
     * @description buttons rendering function
     */
    function renderButtons() {

        let buttonContainer = document.createElement('div');
        buttonContainer.className = 'b-button-container';

        let nextButton = document.createElement('button');
        nextButton.setAttribute('data-action', 'next');
        nextButton.innerHTML = '&raquo;';

        let previousButton = document.createElement('button');
        previousButton.setAttribute('data-action', 'previous');
        previousButton.innerHTML = '&laquo;';

        let sorter = document.createElement('button');
        sorter.setAttribute('data-action', 'sortByPrice');
        sorter.innerText = 'Sort';

        let filterInput = document.createElement('input');
        filterInput.className = 'b-button-container__input';
        filterInput.setAttribute('placeholder', 'Filter ingredients');

        let cartLink = document.createElement('a');
        cartLink.setAttribute('href', '../html/cart.html');
        cartLink.className = 'b-cart-link';

        let cartIcon = document.createElement('i');
        cartIcon.className = 'material-icons';
        cartIcon.innerText = 'shopping_cart';
      /*  cartIcon.style.fontSize = '2vw';*/

        cartLink.appendChild(cartIcon);

        buttonContainer.appendChild(previousButton);
        buttonContainer.appendChild(nextButton);
        buttonContainer.appendChild(sorter);
        buttonContainer.appendChild(filterInput);
        buttonContainer.appendChild(cartLink);

        instance.appendChild(buttonContainer);

        let buttons = instance.querySelectorAll('button');

        for (let i = 0; i < buttons.length; i++) {

            buttons[i].className = 'b-button-container__button';
            buttons[i].style.fontSize = '2vw';
        }

        nextButton.style.position = 'fixed';
        nextButton.style.top = '50vh';
        nextButton.style.right = '5vw';

        previousButton.style.position = 'fixed';
        previousButton.style.top = '50vh';
        previousButton.style.left = '5vw';

        sorter.style.width = '20%';
    }

    /**
     * @description a function to clear all the content
     */
    function clearItems() {

        currentPage = 0;
        document.getElementById('grid').lastChild.innerHTML = "";
    }

    /**
     * @description a function to show current tab
     */
    function show() {

        let visible = document.querySelectorAll('.shown');
        for (let j = 0; j < visible.length; j++) {
            visible[j].style.display = 'block';
        }
    }

    /**
     * @description a function to hide current tab
     */
    function hide() {

        let visible = document.querySelectorAll('.shown');
        for (let j = 0; j < visible.length; j++) {
            visible[j].classList.remove('shown');
            visible[j].style.display = 'none';
        }

    }

    /**
     * @description a function to show the next tab
     */
    function next() {

        if (currentPage < Math.floor(elements.length / itemsNumber) - 1) {

            hide();
            currentPage += 1;
            for (let i = 0; i < elements.length; i++) {
                if (Math.floor(i / itemsNumber) === currentPage) {
                    elements[i].classList.add('shown');
                }
            }
            show();
        }
    }

    /**
     * @description a function to show the previous tab
     */
    function previous() {

        if (currentPage > 0) {

            hide();
            currentPage -= 1;

            for (let i = elements.length - 1; i >= 0; i--) {
                if (Math.floor(i / itemsNumber) === currentPage) {
                    elements[i].classList.add('shown');
                }
            }
            show();
        }
    }

    /**
     * @description a function toggling pizzas' customization features
     */
    function toggleCustom() {

        let customFeatures = instance.getElementsByClassName('custom');

        for (let i = 0; i < customFeatures.length; i++) {
            if (!customFeatures[i].style.display || customFeatures[i].style.display === 'none') {
                customFeatures[i].style.display = 'block';
                customState = true;
            } else {
                customFeatures[i].style.display = 'none';
                customState = false;
            }
        }
        return customState;
    }

    /**
     * @description price sorting function.
     * The order rises from the cheapest price
     */
    function sortByPrice() {

        items.sort((first, next) => {
            return parseInt(first.price) - parseInt(next.price);
        });
        self.clearItems();
        self.renderItems(items);
    }

    /**
     * @description a function to perform complex filtering.
     * Regular expressions are used to distinguish between
     * different ingredients.
     * Case insensitive
     */
    function filterIngredients() {


        let input = document.getElementsByClassName('b-button-container__input');
        let filter = input[0].value.toUpperCase().split(/\s+|,\s*/);

        let transformFilter = function () {

            let stringBuilder = new StringBuilder();
            stringBuilder.append('(');
            for (let i = 0; i < filter.length; i++) {
                stringBuilder.append(filter[i] + "|");
            }
            stringBuilder = stringBuilder.toString().slice(0, -1);
            stringBuilder = new StringBuilder(stringBuilder);
            stringBuilder.append(')');
            let tail = stringBuilder.toString();

            if (filter.length > 1) {
                for (let j = 0; j < filter.length - 1; j++) {
                    stringBuilder.append('.*');
                    stringBuilder.append(tail);
                }
            }
            return stringBuilder.toString();
        };

        let filteredItems = items.filter((item) => item.toString().toUpperCase().search(transformFilter()) > -1);

        self.clearItems();
        self.renderItems(filteredItems);
    }

    /**
     * @description a function to obtain cart instance from localStorage
     * @returns {JSON}
     */
    function getCart() {

        return JSON.parse(storage.getItem('cart'));
    }

    /**
     * @description a function to set a cart object in localStorage
     * @param object
     * @returns {boolean}
     */
    function setCart(object) {

        storage.setItem('cart', JSON.stringify(object));
        return false;
    }

    /**
     * @description a function to add the item to cart
     * @param target an event target
     */
    function addToCart(target) {

        if (!storage && storageAvailable('localStorage')) {

            storage = window.localStorage;
        }

        let cart = getCart() || {};

        let customElement = target.parentElement;
        let originalElement = customElement.previousElementSibling;
        let itemName = originalElement.parentElement.previousElementSibling.innerText;

        let item = {name: itemName, self: this};

        if (!customState) {

            item.price = originalElement.querySelector('.price').innerText.split(`\u00A0`)[1];
            item.calories = originalElement.querySelector('.calories').innerText.split(`\u00A0`)[1];
            item.ingredients = originalElement.querySelector('.ingredients').innerText.split(`\u00A0`)[1];
            item.id = hashCode(originalElement.querySelector('.ingredients').innerText + self.name);
        } else {

            item.price = customElement.children[1].firstElementChild.innerText.split(`\u00A0`)[1];
            item.calories = customElement.children[1].lastElementChild.innerText.split(`\u00A0`)[1];
            item.ingredients = customElement.querySelector('.b-description-container-custom-list__li-ingredients').innerText;
            item.id = hashCode(customElement.querySelector('.b-description-container-custom-list__li-ingredients').innerText + self.name);
        }

        cart[item.id] = item;

        setCart(cart);
    }


    this.createElement = createElement;
    this.renderButtons = renderButtons;
    this.renderItems = renderItems;
    this.clearItems = clearItems;
    this.next = next;
    this.previous = previous;
    this.sortByPrice = sortByPrice;
    this.toggleCustom = toggleCustom;
    this.addToCart = addToCart;
}

let grid = new Grid();
let element = grid.createElement();
grid.renderButtons();
grid.renderItems(initItems());

let footer = document.getElementById('footer');
document.body.insertBefore(element, footer);
/**
 * @author Anton Plotnikov
 * @description {@link List} rendering script
 */

/**
 *
 * @constructor
 */
function List() {

    let instance;
    let self = this;
    let items = initItems();

    /**
     * @description  component initialization
     * @returns {*} new Singleton instance
     */
    function createElement() {

        if (!instance) {
            render();
        }
        return instance;
    }

    /**
     * @description main component rendering function
     */
    function render() {

        instance = document.createElement('section');
        instance.className = 'b-list';
        instance.setAttribute('id', 'list');

        instance.onclick = function (event) {

            if (event.target.closest('.b-list-button-container')) {
                sortByPrice();
            }
        }
    }

    /**
     * @description items rendering function
     * @param items - an array of objects to be represented
     * in list
     */
    function renderItems(items) {

        let ul = document.createElement('ul');
        ul.className = 'b-ul';

        items.forEach((item) => {

            let list = document.createElement('li');
            list.className = 'b-ul__li';
            list.innerText = item.name + ", " + item.price + " uah";

            list.pseudoStyle("before", "content", "''");
            list.pseudoStyle("before", "display", "block");
            list.pseudoStyle("before", "display", "block");
            list.pseudoStyle("before", "background-image", "url('" + item.photo + "')");
            list.pseudoStyle("before", "background-size", "3.5vw");
            list.pseudoStyle("before", "width", "3.5vw");
            list.pseudoStyle("before", "height", "3.5vw");
            list.pseudoStyle("before", "position", "relative");
            list.pseudoStyle("before", "left", "-4.5vw");
            list.pseudoStyle("before", "top", "3vw");

            ul.appendChild(list);
        });


        element.appendChild(ul);
    }

    /**
     * @description buttons rendering function
     */
    function renderButtons() {

        let buttonContainer = document.createElement('div');
        buttonContainer.className = 'b-list-button-container';
        buttonContainer.style.cssText = "width: 50%; \
        margin: 0 auto 20px; \
        padding: 0 0 5% 0;";

        let sorter = document.createElement('button');
        sorter.setAttribute('data-action', 'sortByPrice');
        sorter.innerText = 'Sort';
        sorter.className = 'b-list-button-container__button';

        buttonContainer.appendChild(sorter);

        instance.appendChild(buttonContainer);
    }

    /**
     * @description a function to clear all the content
     */
    function clearItems() {

        document.getElementById('list').innerHTML = "";
    }

    /**
     * @description price sorting function.
     * The order rises from the cheapest price
     */
    function sortByPrice() {

        items.sort((first, next) => {
            return parseInt(first.price) - parseInt(next.price);
        });
        self.clearItems();
        self.renderItems(items);
    }

    this.createElement = createElement;
    this.renderButtons = renderButtons;
    this.renderItems = renderItems;
    this.clearItems = clearItems;
    this.sortByPrice = sortByPrice;
}

let list = new List();
let element = list.createElement();
list.renderItems(initItems());
list.renderButtons();

let footer = document.getElementById('footer');
document.body.insertBefore(element, footer);
/**
 * @author Anton Plotnikov
 * @description {@link Pizza} initializing script
 */

/**
 *
 * @param options a list of options
 * @constructor
 */
function Pizza(options) {

    let self = this;
    this.id = options.id;
    this.photo = options.photo;
    this.name = options.name;
    this.ingredients = options.ingredients;
    this.price = setPrice();
    this.calories = setCalories();

    this.toString = function () {

        let pizzaString = "";

        for (let i = 0; i < self.ingredients.length; i++) {
            pizzaString += self.ingredients[i].name + ", ";
        }

        return pizzaString.slice(0, -2);
    };

    function setPrice() {

        let price = 50;

        for (let i = 0; i < self.ingredients.length; i++) {
            price += self.ingredients[i].price;
        }

        return price;
    }

    function setCalories() {

        let calories = 0;

        for (let i = 0; i < self.ingredients.length; i++) {
            calories += self.ingredients[i].calories;
        }

        return calories;
    }

}


/**
 * @description map containers to maintain {@link Pizza} properties
 * @type {Map<number, string>}
 */
let names = new Map();
let photos = new Map();
let ingredients = new Map();
let compounds = new Map();

names.set(1, "Hawaiian")
    .set(2, "Neapolitan")
    .set(3, "Margarita")
    .set(4, "Diabola")
    .set(5, "Sicilian")
    .set(6, "Marinara")
    .set(7, "Capricciosa")
    .set(8, "Four seasons")
    .set(9, "Cheese")
    .set(10, "Primavera")
    .set(11, "Bavarian")
    .set(12, "Romana")
    .set(13, "Viennese")
    .set(14, "Bianca")
    .set(15, "Funghi")
    .set(16, "Vegetariana")
    .set(17, "Maltese")
    .set(18, "Rucola");

photos.set(1, "../images/Hawaii.png")
    .set(2, "../images/Neapolitan.png")
    .set(3, "../images/Margarita.png")
    .set(4, "../images/Diabola.png")
    .set(5, "../images/Sicilian.png")
    .set(6, "../images/Marinara.png")
    .set(7, "../images/Capricciosa.png")
    .set(8, "../images/FourSeasons.png")
    .set(9, "../images/Cheese.png")
    .set(10, "../images/Primavera.png")
    .set(11, "../images/Bavarian.png")
    .set(12, "../images/Romana.png")
    .set(13, "../images/Viennese.png")
    .set(14, "../images/Bianca.png")
    .set(15, "../images/Funghi.png")
    .set(16, "../images/Vegetariana.png")
    .set(17, "../images/Maltese.png")
    .set(18, "../images/Rucola.png");

compounds.set("Pineapple", {name: "Pineapple", price: 30, calories: 30})
    .set("Ham", {name: "Ham", price: 40, calories: 150})
    .set("Cheese", {name: "Cheese", price: 20, calories: 250})
    .set("Tomato sauce", {name: "Tomato sauce", price: 10, calories: 40})
    .set("Tomatoes", {name: "Tomatoes", price: 20, calories: 30})
    .set("Anchovies", {name: "Anchovies", price: 40, calories: 100})
    .set("Mozzarella", {name: "Mozzarella", price: 40, calories: 250})
    .set("Parmesan", {name: "Parmesan", price: 40, calories: 250})
    .set("Basil", {name: "Basil", price: 15, calories: 20})
    .set("Salami", {name: "Salami", price: 45, calories: 200})
    .set("Chili", {name: "Chili", price: 15, calories: 10})
    .set("Olives", {name: "Olives", price: 25, calories: 180})
    .set("Pecorino cheese", {name: "Pecorino cheese", price: 50, calories: 280})
    .set("Shrimps", {name: "Shrimps", price: 20, calories: 120})
    .set("Tuna", {name: "Tuna", price: 25, calories: 110})
    .set("Calamari", {name: "Calamari", price: 35, calories: 90})
    .set("Mussels", {name: "Mussels", price: 25, calories:100})
    .set("Crab meat", {name: "Crab meat", price: 25, calories: 100})
    .set("Boiled egg", {name: "Boiled egg", price: 10, calories: 80})
    .set("Olive oil", {name: "Olive oil", price: 10, calories: 100})
    .set("Mushrooms", {name: "Mushrooms", price: 15, calories: 50})
    .set("Pepper", {name: "Pepper", price: 15, calories: 50})
    .set("Artichokes", {name: "Artichokes", price: 30, calories: 80})
    .set("Ricotta", {name: "Ricotta", price: 20, calories: 250})
    .set("Gorgonzola", {name: "Gorgonzola", price: 35, calories: 250})
    .set("Oregano", {name: "Oregano", price: 20, calories: 60})
    .set("Bavarian sausages", {name: "Bavarian sausages", price: 20, calories: 190})
    .set("Rosemary", {name: "Rosemary", price: 15, calories: 10})
    .set("Salt", {name: "Salt", price: 5, calories: 0})
    .set("Onions", {name: "Onions", price: 10, calories: 35})
    .set("Sweet corn", {name: "Sweet corn", price: 15, calories: 50})
    .set("Goat cheese", {name: "Goat cheese", price: 25, calories: 200})
    .set("Maltese sausage", {name: "Maltese sausage", price: 25, calories: 200})
    .set("Rucola", {name: "Rucola", price: 10, calories: 10});

ingredients.set(1, [
    compounds.get("Pineapple"),
    compounds.get("Ham"),
    compounds.get("Cheese"),
    compounds.get("Tomato sauce")])
    .set(2, [
        compounds.get("Tomatoes"),
        compounds.get("Anchovies"),
        compounds.get("Mozzarella"),
        compounds.get("Parmesan"),
        compounds.get("Basil")])
    .set(3, [
        compounds.get("Mozzarella"),
        compounds.get("Tomato sauce")])
    .set(4, [
        compounds.get("Salami"),
        compounds.get('Chili'),
        compounds.get("Olives")])
    .set(5, [
        compounds.get("Anchovies"),
        compounds.get("Pecorino cheese"),
        compounds.get("Tomato sauce")])
    .set(6, [
        compounds.get("Tomato sauce"),
        compounds.get("Mozzarella"),
        compounds.get("Shrimps"),
        compounds.get("Mussels"),
        compounds.get("Tuna"),
        compounds.get("Calamari"),
        compounds.get("Crab meat")
    ])
    .set(7, [
        compounds.get("Ham"),
        compounds.get("Olives"),
        compounds.get("Boiled egg"),
        compounds.get("Mushrooms")
    ])
    .set(8, [
        compounds.get("Tomatoes"),
        compounds.get("Mushrooms"),
        compounds.get("Pepper"),
        compounds.get("Artichokes")
    ])
    .set(9, [
        compounds.get("Parmesan"),
        compounds.get("Mozzarella"),
        compounds.get("Basil"),
        compounds.get("Ricotta"),
        compounds.get("Gorgonzola")
    ])
    .set(10, [
        compounds.get("Mozzarella"),
        compounds.get("Oregano"),
        compounds.get("Olive oil"),
        compounds.get("Tomato sauce")
    ])
    .set(11, [
        compounds.get("Mushrooms"),
        compounds.get("Tomatoes"),
        compounds.get("Bavarian sausages"),
        compounds.get("Mozzarella")
    ])
    .set(12, [
        compounds.get("Tomatoes"),
        compounds.get("Mozzarella"),
        compounds.get("Anchovies"),
        compounds.get("Oregano"),
        compounds.get("Olive oil")
    ])
    .set(13, [
        compounds.get("Tomatoes"),
        compounds.get("Mozzarella"),
        compounds.get("Bavarian sausages"),
        compounds.get("Oregano"),
        compounds.get("Olive oil")
    ])
    .set(14, [
        compounds.get("Olive oil"),
        compounds.get("Rosemary"),
        compounds.get("Salt")
    ])
    .set(15, [
        compounds.get("Tomato sauce"),
        compounds.get("Mozzarella"),
        compounds.get("Mushrooms")
    ])
    .set(16, [
        compounds.get("Tomato sauce"),
        compounds.get("Mozzarella"),
        compounds.get("Mushrooms"),
        compounds.get("Onions"),
        compounds.get("Artichokes"),
        compounds.get("Sweet corn"),
        compounds.get("Pepper")
    ])
    .set(17, [
        compounds.get("Tomato sauce"),
        compounds.get("Goat cheese"),
        compounds.get("Tomatoes"),
        compounds.get("Maltese sausage"),
        compounds.get("Onions")
    ])
    .set(18, [
        compounds.get("Tomato sauce"),
        compounds.get("Mozzarella"),
        compounds.get("Ham"),
        compounds.get("Parmesan"),
        compounds.get("Rucola")
    ]);

/**
 * @description {@link Pizza} objects initialization
 * @returns {Array}
 */
function initItems() {

    let pizzas = [];

    for (let i = 0; i < names.size; i++) {

        let counter = i + 1;
        pizzas[i] = new Pizza({
            id: counter,
            name: names.get(counter),
            photo: photos.get(counter),
            ingredients: ingredients.get(counter)
        });
    }
    return pizzas;
}
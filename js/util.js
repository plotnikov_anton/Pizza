/**
 * @author Anton Plotnikov
 * @description StringBuilder class for string concatenation simplification
 */
function StringBuilder(value) {
    this.strings = new Array("");
    this.append(value);
}

/**
 * Appends the given value to the end of this instance
 */
StringBuilder.prototype.append = function (value) {
    if (value) {
        this.strings.push(value);
    }
};

/**
 * Clears the string buffer
 */
StringBuilder.prototype.clear = function () {
    this.strings.length = 1;
};

/**
 * Converts this instance to a String
 */
StringBuilder.prototype.toString = function () {
    return this.strings.join("");
};

/**
 * @author Andrew McGivery
 * See {@link http://mcgivery.com}
 */

/**
 *
 * @type {{current: number, getNew: UID.getNew}}
 */
let UID = {
    current: 0,
    getNew: function () {
        this.current++;
        return this.current;
    }
};

/**
 *
 * @param element
 * @param prop
 * @param value
 * @returns {HTMLElement}
 */
HTMLElement.prototype.pseudoStyle = function (element, prop, value) {

    let self = this;
    let sheetId = "pseudoStyles";
    let head = document.head || document.getElementsByTagName('head')[0];
    let sheet = document.getElementById(sheetId) || document.createElement('style');
    sheet.id = sheetId;
    let className = "pseudoStyle" + UID.getNew();

    self.className += " " + className;

    sheet.innerHTML += " ." + className + ":" + element + "{" + prop + ":" + value + "}";
    head.appendChild(sheet);
    return this;
};

/**
 * See {@link https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API}
 *
 * @description Storage availability testing function
 * @param type 'localStorage' or 'sessionStorage' strings
 * @returns {boolean} pointing out whether the Storage available
 */
function storageAvailable(type) {

    let storage = window[type];

    try {

        let x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch (e) {

        return e instanceof DOMException && (
                // everything except Firefox
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // test name field too, because code might not be present
            // everything except Firefox
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            storage.length !== 0;
    }
}

function hashCode(string) {

    let hash, i;

    for (hash = i = 0; i < string.length; ++i) {
        hash += string.charCodeAt(i);
        hash += (hash << 10);
        hash ^= (hash >> 6);
    }
    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);

    return hash;
}